# NanoGame

A small side scroller game made with rust



## Development

### Build

```
chmod +x build.sh
./build.sh
```



### Install

Download Sipeed's toolchain utility from
[here](https://dl.sipeed.com/LONGAN/platformio/dl-packages/) and add directory
of riscv-nuclei-elf-objcopy into PATH (in .bashrc: export PATH...).

For installing binary to target device, specific version of dfu-utils is
needed.  Some guides tell to download Sipeed's own version, but apparently
v0.10 is the only one that works. This can be either built from source
([instructions]( http://dfu-util.sourceforge.net/build.html)) (tested with
fedora 33) or installed using package manager. AFAIK only arch has the correct
version.

when usb port is pointing down:
right - BOOT,
left - RESET

1. Connect device with USB-C cable
2. On device: 
    press & hold BOOT, 
    press & hold RESET, 
    release RESET, 
    release BOOT

3. Run install.sh (chmod +x install.sh, if needed). Flashing is successful,
although it says 
```
Download done.  
File downloaded successfully  
dfu-util: Error during download get_status
```
4. Disconnect USB-C cable, wait 2 s and re-connect it. This will ensure the new
code is actually being run.



### Other usage

Longan nano isn't recognized as USB device on linux. For serial comm, JTAG is
needed. Serial device is then either /dev/ttyUSB1 or /dev/ttyUSB0.



## Gameplay


- Avoid incoming obstacles by jumping over them. 
- Press BOOT to jump up to quadruple jump. 
- For new game, press RESET.


#!/bin/sh

TARGET="nanogame"
TARGET_PATH=target/riscv32imac-unknown-none-elf/release/$TARGET

cargo build --release && \
riscv-nuclei-elf-objcopy -O binary "$TARGET_PATH" "$TARGET".bin

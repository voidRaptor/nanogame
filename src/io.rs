use gd32vf103xx_hal::pac;
use gd32vf103xx_hal::prelude::*;
use gd32vf103xx_hal::serial::Tx;
use gd32vf103xx_hal::gpio;
use gd32vf103xx_hal::gpio::gpioa::PA8;

use embedded_hal::digital::v2::InputPin;



pub fn send_char(tx: &mut Tx<pac::USART0>, c: u8) {
    loop {
        let result = tx.write(c).ok();
        match result {
            Some(_x) => break,
            None    => continue,
        }
    }
}


pub fn send_str(tx: &mut Tx<pac::USART0>, s: &str) {

    for c in s.chars() {
        send_char(tx, c as u8);
    }
}


pub fn button_pressed(button: &PA8<gpio::Input<gpio::Floating>>) -> bool {
    button.is_high().unwrap()
}






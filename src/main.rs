#![no_std]
#![no_main]

#![allow(dead_code)]
#![allow(unused_mut)]
#![allow(unused_variables)]
#![allow(unused_imports)]

const FPS: u32 = 10;
const FRAME_TIME: u32 = (1.0_f32 / FPS as f32 * 1000.0) as u32;

use panic_halt as _;

use riscv_rt::entry;

use gd32vf103xx_hal::serial::Serial;
use gd32vf103xx_hal::serial::Config;
use gd32vf103xx_hal::serial::StopBits;
use gd32vf103xx_hal::serial::Parity;

use gd32vf103xx_hal::pac;
use gd32vf103xx_hal::prelude::*;
use gd32vf103xx_hal::delay::McycleDelay;

use longan_nano::lcd_pins;
use longan_nano::lcd::Lcd;
use longan_nano::led::{Led, rgb};

use embedded_hal::blocking::delay::DelayMs;

use embedded_graphics::prelude::*;
use embedded_graphics::pixelcolor::Rgb565;
use embedded_graphics::primitives::Rectangle;
use embedded_graphics::fonts::Font12x16;

mod utility;
mod io;


const WHITE:  u16 = 0xFFFF;
const BLACK:  u16 = 0x0000;
const GREEN:  u16 = 0x07E0;
const BLUE:   u16 = 0x001F;

// obstacle colors
const PURPLE: u16 = 0xF81F;
const L_BLUE: u16 = 0x07FF;
const YELLOW: u16 = 0xFFE0;
const ORANGE: u16 = 0xFBE0;
const RED:    u16 = 0xF800;

const CLEAR_COLOR: u16 = GREEN;

const GRAVITY: utility::Vec2 = utility::Vec2{x: 0, y: 4};
const SCREEN: utility::Rect = utility::Rect{x:0, y:0, w:160, h:80};

const PADDING:     i32 = 2;
const BLOCK_SIZE:  i32 = 5;
const PLAYER_SIZE: i32 = BLOCK_SIZE * 4;
const START_X:     i32 = PADDING;
const START_Y:     i32 = PADDING;
const GROUND_LVL:  i32 = SCREEN.h - PADDING;

const MAX_VELOCITY: utility::Vec2 = utility::Vec2 {x: 0, y: 40};
const MIN_VELOCITY: utility::Vec2 = utility::Vec2 {x: 0, y: -10};

const JUMP_DURATION: u32 = 4;

const HEIGHTS: &[i32] =
&[
    20, 20, 40, 20, 30,
    30, 40, 45, 10, 50,
    10, 25, 15, 35, 30,
    40, 40, 45, 50, 55,
];



// TODO EXTRA
// - new obs types: double, triple -> tail size constant unless special obs
// - sounds
// - replace counters with real timers

#[entry]
fn main() -> ! {
    // create handle for peripherals
    let p = pac::Peripherals::take().unwrap();

    // configure clocks
    let mut rcu = p.RCU.configure().ext_hf_clock(8.mhz()).sysclk(108.mhz()).freeze();

    let gpioa = p.GPIOA.split(&mut rcu);
    let gpiob = p.GPIOB.split(&mut rcu);
    let gpioc = p.GPIOC.split(&mut rcu);

    let (mut red, mut green, mut blue) = rgb(gpioc.pc13, gpioa.pa1, gpioa.pa2);
    let mut leds: [&mut dyn Led; 3] = [&mut red, &mut green, &mut blue];

    let lcd_pins = lcd_pins!(gpioa, gpiob);
    let mut lcd = Lcd::new(p.SPI0, lcd_pins, &mut rcu);
    let mut delay = McycleDelay::new(&rcu.clocks);

    // blacken leds
    for c in &mut leds {
        c.off();
    }

    let boot_button  = gpioa.pa8.into_floating_input();
    let pin_tx = gpioa.pa9.into_alternate_push_pull();
    let pin_rx = gpioa.pa10.into_floating_input();

    let serial = Serial::usart0(
        p.USART0,
        (pin_tx, pin_rx),

        Config {
            baudrate: 9600.bps(),
            parity:   Parity::ParityNone,
            stopbits: StopBits::STOP1
        },

        &mut rcu,
    );

    // separate into tx and rx channels
    let (mut tx, mut _rx) = serial.split();


    let mut player = Player {
        accel:    utility::Vec2{x: 0, y: 0},
        velocity: utility::Vec2{x: 0, y: 0},

        area: utility::Rect{
            x: START_X,
            y: START_Y,
            w: PLAYER_SIZE,
            h: PLAYER_SIZE
        },

        color: BLUE,
        state: PlayerState::GROUND,
        jump_counter: 0,
    };



    let mut obstacle = Obstacle {
        velocity: utility::Vec2{x: -10, y: 0},

        area: utility::Rect{
            x: SCREEN.w - PADDING - PLAYER_SIZE,
            y: SCREEN.h - PADDING - HEIGHTS[0],
            w: PLAYER_SIZE,
            h: HEIGHTS[0],
        },

        color: RED,
        tail: 0,  // unused
        obstacle_index: 0,
    };

    obstacle.color = size_to_color(&obstacle.area);

    let mut score: u32 = 0;

    draw_rect(&mut lcd, &SCREEN, CLEAR_COLOR);

    loop {
        player.update();
        player.jump( io::button_pressed(&boot_button) );
        obstacle.update();

        if utility::has_intersection(&player.area, &obstacle.area) {
            gameover(&mut lcd,
                     &mut leds,
                     &mut delay,
                     &mut player,
                     &mut obstacle,
                     &mut score);
        }

        obstacle.respawn(&mut score);

        player.draw(&mut lcd);
        obstacle.draw(&mut lcd);
        draw_score(&mut lcd, score);


        delay.delay_ms(FRAME_TIME);

        player.clear(&mut lcd);
        obstacle.clear(&mut lcd);
    }
}


pub trait String {
    fn to_str(&self) -> &str;
}


fn render_num(lcd: &mut Lcd, num: u8, x: i32, y: i32) {
    let string = utility::to_str(num);

    let t = Font12x16::render_str(&string)
        .stroke(Some(YELLOW.into()))
        .fill(Some(RED.into()))
        .translate( Coord::new( x, y) );

    lcd.draw(t);
}



fn draw_score(lcd: &mut Lcd, score: u32) {
    let x: i32 = SCREEN.x + SCREEN.w;
    let y: i32 = SCREEN.y;

    render_num(lcd, utility::get_hundreds(score), x - 36, y);
    render_num(lcd, utility::get_tens(score), x - 24, y);
    render_num(lcd, utility::get_ones(score), x - 12, y);
}


fn draw_rect<C>(lcd: &mut Lcd, rect: &utility::Rect, c: C) where C: Into<Rgb565> {
    lcd.draw(Rectangle::new(
        Coord::new(rect.x, rect.y),
        Coord::new(rect.x + rect.w, rect.y + rect.h),
    )
    .fill(Some(c.into())));
}


fn gameover(lcd: &mut Lcd,
            leds: &mut [&mut dyn Led; 3],
            delay: &mut McycleDelay,
            player: &mut Player,
            obstacle: &mut Obstacle,
            score: &mut u32) -> ! {

    player.draw(lcd);
    obstacle.draw(lcd);

    leds[0].off();
    leds[1].off();
    leds[2].on();

    delay.delay_ms(10*FRAME_TIME);

    // screen swipe animation
    let mut clear_rect = utility::Rect{
        x: SCREEN.w-1,
        y: SCREEN.y,
        w: 1,
        h: SCREEN.h
    };

    for _i in (0..SCREEN.w).rev() {
        draw_rect(lcd, &clear_rect, BLACK);

        clear_rect.x -= 1;
    }

    delay.delay_ms(5*FRAME_TIME);


    let t = Font12x16::render_str("GAME OVER")
        .stroke(Some(YELLOW.into()))
        .fill(Some(RED.into()))
        .translate(Coord::new(27, 20));

    lcd.draw(t);


    let t = Font12x16::render_str("score")
        .stroke(Some(YELLOW.into()))
        .fill(Some(RED.into()))
        .translate(Coord::new(27, 51));

    lcd.draw(t);

    // len of "score " + width of previous characters in the line
    render_num(lcd, utility::get_hundreds(*score), 27 + 12*6, 50);
    render_num(lcd, utility::get_tens(*score),     27 + 12*7, 50);
    render_num(lcd, utility::get_ones(*score),     27 + 12*8, 50);

    loop {}

}


fn edges(rect: &mut utility::Rect) {

    // clip
    if rect.y + rect.h > SCREEN.h - PADDING {
        rect.y = SCREEN.h - rect.h - PADDING;
    }

    if rect.y < START_Y {
        rect.y = START_Y;
    }

}


enum PlayerState {
    GROUND,
    JUMP,
}

pub struct Player {
    velocity: utility::Vec2,
    accel: utility::Vec2,
    area: utility::Rect,  // xy = top left corner
    color: u16,
    state: PlayerState,
    jump_counter: u32,

}


pub trait GameObject {

    // update position and physics
    fn update(&mut self);

    // add accelerative force
    fn add_force(&mut self, force: &utility::Vec2);

    // draw on lcd
    fn draw(&self, lcd: &mut Lcd);

    // clear only this object from screen
    fn clear(&self, lcd: &mut Lcd);
}


impl GameObject for Player {

    fn update(&mut self) {

        // update state
        if self.area.y + self.area.h == GROUND_LVL {
            self.state = PlayerState::GROUND;

        } else {
            self.state = PlayerState::JUMP;
        }


        if let PlayerState::GROUND = self.state {
            self.velocity.y = 0;
        }

        self.velocity.x += self.accel.x + GRAVITY.x;
        self.velocity.y += self.accel.y + GRAVITY.y;

        // TODO: limit based on fps since "jamming" effect depends on
        // that and edges's impl
        utility::limit_vec(&mut self.velocity, &MIN_VELOCITY, &MAX_VELOCITY);


        self.area.x += self.velocity.x;
        self.area.y += self.velocity.y;


        edges(&mut self.area);


    }

    fn add_force(&mut self, force: &utility::Vec2) {
        self.velocity.x += force.x;
        self.velocity.y += force.y;
    }

    fn draw(&self, lcd: &mut Lcd) {
        draw_rect(lcd, &self.area, self.color);
    }

    fn clear(&self, lcd: &mut Lcd) {
        draw_rect(lcd, &self.area, CLEAR_COLOR);
    }


}



impl Player {

    fn jump(&mut self, button_pressed: bool) {

        if button_pressed && self.jump_counter < JUMP_DURATION {
            self.accel = utility::Vec2{x: 0, y: -40};
            self.jump_counter += 1;

        } else {
            self.accel = utility::Vec2{x: 0, y: 0};
        }


        if let PlayerState::GROUND = self.state {
            self.velocity.y = 0;

            if self.jump_counter > 1 {
                self.jump_counter -= 1;
            }

        }

    }


}


struct Obstacle {
    velocity: utility::Vec2,
    area: utility::Rect,  // xy = top left corner
    color: u16,
    tail: i32, // empty space after (pixels)
    obstacle_index: u32 ,
}


impl GameObject for Obstacle {

    fn update(&mut self) {
        self.area.x += self.velocity.x;
        self.area.y += self.velocity.y;
    }

    fn add_force(&mut self, force: &utility::Vec2) {
        self.velocity.x += force.x;
        self.velocity.y += force.y;
    }

    fn draw(&self, lcd: &mut Lcd) {
        draw_rect(lcd, &self.area, self.color);
    }

    fn clear(&self, lcd: &mut Lcd) {
        draw_rect(lcd, &self.area, CLEAR_COLOR);
    }

}


impl Obstacle {

    // set this object as new obstacle without actually creating new
    fn respawn(&mut self, score: &mut u32) {

        if self.area.x < SCREEN.x {
            self.area.x = SCREEN.w + 20;

            // cycle index
            self.obstacle_index = utility::modulus(
                self.obstacle_index + 1,
                HEIGHTS.len() as u32
            );

            self.area.h = HEIGHTS[self.obstacle_index as usize];
            self.area.y = SCREEN.h - PADDING - self.area.h;

            *score += 1;


            self.color = size_to_color(&self.area);
        }
    }

    fn respawn_extra(&mut self, new_height: i32, new_tail: i32) {

        if (SCREEN.w - self.area.x + self.area.w) > self.tail {
            self.area.x = SCREEN.w + 20;
            self.area.h = new_height;
            self.tail = new_tail;
        }
    }

}


fn size_to_color(area: &utility::Rect) -> u16 {

    match area.h {
        0..=10  => PURPLE,
        11..=20 => L_BLUE,
        21..=30 => YELLOW,
        31..=40 => RED,
        41..=50 => BLACK,
        _       => WHITE,
    }
}



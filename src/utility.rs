
pub struct Rect {
    pub x: i32,
    pub y: i32,
    pub w: i32,
    pub h: i32,
}


pub struct Vec2 {
    pub x: i32,
    pub y: i32,
}


// adapted from SDL_HasIntersection
pub fn has_intersection(a: &Rect, b: &Rect) -> bool {

    // TODO: special cases for empty rects

    // horizontal intersection
    let mut a_min: i32 = a.x;
    let mut a_max: i32 = a_min + a.w;
    let mut b_min: i32 = b.x;
    let mut b_max: i32 = b_min + b.w;

    if b_min > a_min { a_min = b_min; }
    if b_max < a_max { a_max = b_max; }
    if a_max <= a_min { return false; }

    // vertical intersection
    a_min = a.y;
    a_max = a_min + a.h;
    b_min = b.y;
    b_max = b_min + b.h;

    if b_min > a_min { a_min = b_min; }
    if b_max < a_max { a_max = b_max; }
    if a_max <= a_min { return false; }

    true
}


pub fn limit_vec_by_i32(vec: &mut Vec2, min: i32, max: i32) {

    if vec.x > max { vec.x = max; }
    if vec.x < min { vec.x = min; }
    if vec.y > max { vec.y = max; }
    if vec.y < min { vec.y = min; }
}


pub fn limit_vec(vec: &mut Vec2, min: &Vec2, max: &Vec2) {

    if vec.x > max.x { vec.x = max.x; }
    if vec.y > max.y { vec.y = max.y; }

    if vec.x < min.x { vec.x = min.x; }
    if vec.y < min.y { vec.y = min.y; }
}


pub fn char_to_int(data: u8) -> u8 {
    data - 48
}


pub fn int_to_char(data: u8) -> u8 {
    data + 48
}


pub fn modulus(a: u32, b: u32) -> u32 {
    ((a % b) + b) % b
}


pub fn to_str(num: u8) -> &'static str {
    match num {

        0 => "0",
        1 => "1",
        2 => "2",
        3 => "3",
        4 => "4",
        5 => "5",
        6 => "6",
        7 => "7",
        8 => "8",
        9 => "9",
        _ => "?",
    }
}


pub fn get_ones(x: u32) -> u8 {
    modulus(x, 10) as u8
}


pub fn get_tens(x: u32) -> u8 {
    (modulus(x, 100) / 10) as u8
}


pub fn get_hundreds(x: u32) -> u8 {
    (modulus(x, 1000) / 100) as u8
}


fn pack_channels(r: u16, g: u16, b: u16) -> u16 {
    // max red   0x1F
    // max green 0x3F
    // max blue  0x1F

    (r << 11) | (g << 5) | b
}
